#ifndef WIDGET_H
#define WIDGET_H

#include <QOpenGLWidget>
#include <QTimer>
#include "cylinderrender.h"
class Widget : public QOpenGLWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

protected:
    void paintGL() override;
    void resizeGL(int w,int h) override;
    void initializeGL() override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

private:
    QMatrix4x4 pMatrix_;
    QVector3D lightLocation_,camera_;
    CylinderRender render_;
    QTimer tm;
    qreal angleX_ = 0,angleY_ = 0,angleZ_ = 0;

private slots:
    void slotTimeout();
};

#endif // WIDGET_H
