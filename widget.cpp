#include <QTimer>
#include "widget.h"

Widget::Widget(QWidget *parent)
    : QOpenGLWidget(parent)
{
    connect(&tm,SIGNAL(timeout()),this,SLOT(slotTimeout()));
    tm.start(40);
}

Widget::~Widget()
{

}

void Widget::paintGL()
{
    QOpenGLExtraFunctions *f = QOpenGLContext::currentContext()->extraFunctions();
    f->glClearColor(0.0,0.0,0.0,1.0);
    f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    QMatrix4x4 vMatrix;
    vMatrix.lookAt(camera_,QVector3D(0,0,0),QVector3D(0,1,0));

    QMatrix4x4 mMatrix;
    mMatrix.rotate(angleX_,1,0,0);
    mMatrix.rotate(angleY_,0,1,0);
    mMatrix.rotate(angleZ_,0,0,1);
    render_.render(f,pMatrix_,vMatrix,mMatrix,lightLocation_,camera_);
}

void Widget::resizeGL(int w, int h)
{
    pMatrix_.setToIdentity();
    pMatrix_.perspective(45,float(w)/h,0.01f,100.0f);
}

void Widget::initializeGL()
{
    render_.initsize(QImage("rect.jpg"),QImage("top.jpg"),QImage("bottom.jpg"),0.8,0.8);
    camera_.setX(0);
    camera_.setY(0);
    camera_.setZ(3);
    lightLocation_.setX(5);
    lightLocation_.setY(2);
    lightLocation_.setZ(1);
}

void Widget::mouseMoveEvent(QMouseEvent *event)
{
    Q_UNUSED(event)
    angleX_ += 5;
    angleY_ += 5;
    angleZ_ += 5;
    update();
}

void Widget::mousePressEvent(QMouseEvent *event)
{
    tm.stop();
}

void Widget::mouseReleaseEvent(QMouseEvent *event)
{
    tm.start();
}

void Widget::slotTimeout()
{
    angleX_ += 5;
    angleY_ += 5;
    angleZ_ += 5;
    update();
}
