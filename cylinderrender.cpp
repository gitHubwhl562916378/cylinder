#include "cylinderrender.h"

void CylinderRender::initsize(QImage &ce, QImage &top, QImage &bottom, float r, float h)
{
    program_.addCacheableShaderFromSourceFile(QOpenGLShader::Vertex,"vsrc.vert");
    program_.addCacheableShaderFromSourceFile(QOpenGLShader::Fragment,"fsrc.frag");
    program_.link();

    topTexture_ = new QOpenGLTexture(top);
    bottomTexture_ = new QOpenGLTexture(bottom);
    ceTexture_ = new QOpenGLTexture(ce);
    topTexture_->setWrapMode(QOpenGLTexture::ClampToEdge);
    topTexture_->setMinMagFilters(QOpenGLTexture::NearestMipMapNearest,QOpenGLTexture::LinearMipMapNearest);
    bottomTexture_->setWrapMode(QOpenGLTexture::ClampToEdge);
    bottomTexture_->setMinMagFilters(QOpenGLTexture::NearestMipMapNearest,QOpenGLTexture::LinearMipMapNearest);
    ceTexture_->setWrapMode(QOpenGLTexture::ClampToEdge);
    ceTexture_->setMinMagFilters(QOpenGLTexture::NearestMipMapNearest,QOpenGLTexture::LinearMipMapNearest);
    float angleSpan = 5;
    topVec << 0 << h/2 << 0;
    topTexVec << 0.5 << 0.5;
    topNorVec << 0 << 1 << 0;
    bottomVec << 0 << -h/2 << 0;
    bottomTexVec << 0.5 << 0.5;
    bottomNorVec << 0 << -1 << 0;
    for(float angle = 0; angle <= 360; angle += angleSpan){
        //侧面
        float curRad = angle * PI / 180;
        float x1 = r * ::cos(curRad);
        float y1 = - h/2;
        float z1 = r * ::sin(curRad);
        float hx1 = (angle/360);
        float hy1 = 0;
        float nx1 = x1;
        float ny1 = 0;
        float nz1 = z1;

        float x2 = x1;
        float y2 = h/2;
        float z2 = z1;
        float hx2 = hx1;
        float hy2 = 1;
        float nx2 = x2;
        float ny2 = 0;
        float nz2 = z2;

        float nextRad = angle + angleSpan;
        float x3 = r * ::cos(nextRad * PI / 180);
        float y3 = h/2;
        float z3 = r * ::sin(nextRad * PI / 180);
        float hx3 = (nextRad/360);
        float hy3 = 1;
        float nx3 = x3;
        float ny3 = 0;
        float nz3 = z3;

        float x4 = x3;
        float y4 = -h/2;
        float z4 = z3;
        float hx4 = hx3;
        float hy4 = 0;
        float nx4 = x4;
        float ny4 = 0;
        float nz4 = z4;
        ceVec << x1 << y1 << z1 << x2 << y2 << z2 << x3 << y3 << z3 << x4 << y4 << z4;
        ceTextVec << hx2 << hy2  << hx1 << hy1<< hx4 << hy4 << hx3 << hy3;
        ceNorVec << nx1 << ny1 << nz1 << nx2 << ny2 << nz2 << nx3 << ny3 << nz3 << nx4 << ny4 << nz4;
        //顶面
        x2 = r * ::cos(-curRad);
        z2 = r * ::sin(-curRad);
        topVec << x2 << y2 << z2;
        float topTx1 = 0.5 - 0.5 * ::cos(curRad);
        float topTy1 = 0.5 - 0.5 * ::sin(-curRad);
        topTexVec << topTx1 << topTy1;
        topNorVec << 0 << 1 << 0;
        //底面
        bottomVec << x1 << y1 << z1;
        bottomTexVec << topTx1 << topTy1;
        bottomNorVec << 0 << -1 << 0;
    }
    vertVec_ << ceVec  << topVec << bottomVec;
    textVec_ << ceTextVec << topTexVec << bottomTexVec;
    normalVec_ << ceNorVec << topNorVec << bottomNorVec;

    QVector<GLfloat> bytesVec;
    bytesVec << vertVec_ << textVec_ << normalVec_;
    vbo_.create();
    vbo_.bind();
    vbo_.allocate(bytesVec.data(),bytesVec.count() * sizeof GLfloat);
}

void CylinderRender::render(QOpenGLExtraFunctions *f, QMatrix4x4 &pMatrix, QMatrix4x4 &vMatrix, QMatrix4x4 &mMatrix, QVector3D &light, QVector3D &camera)
{
    f->glEnable(GL_DEPTH_TEST);
    f->glEnable(GL_CULL_FACE);

    program_.bind();
    vbo_.bind();
    f->glActiveTexture(GL_TEXTURE0 + 0);
    program_.setUniformValue("uPMatrix",pMatrix);
    program_.setUniformValue("uVMatrix",vMatrix);
    program_.setUniformValue("uMMatrix",mMatrix);
    program_.setUniformValue("uLightLocation",light);
    program_.setUniformValue("uCamera",camera);
    program_.setUniformValue("sTextures",0);

    program_.enableAttributeArray(0);
    program_.enableAttributeArray(1);
    program_.enableAttributeArray(2);
    program_.setAttributeBuffer(0,GL_FLOAT,0,3,3*sizeof(GLfloat));
    program_.setAttributeBuffer(1,GL_FLOAT,vertVec_.count() * sizeof(GLfloat),2,2*sizeof(GLfloat));
    program_.setAttributeBuffer(2,GL_FLOAT,(vertVec_.count() + textVec_.count())*sizeof(GLfloat),3,3*sizeof(GLfloat));
    ceTexture_->bind();
    f->glDrawArrays(GL_QUADS,0,ceVec.count()/3);
    topTexture_->bind();
    f->glDrawArrays(GL_TRIANGLE_FAN,ceVec.count()/3,topVec.count()/3);
    bottomTexture_->bind();
    f->glDrawArrays(GL_TRIANGLE_FAN,(ceVec.count() + topVec.count())/3,bottomVec.count()/3);

    program_.disableAttributeArray(0);
    program_.disableAttributeArray(1);
    program_.disableAttributeArray(2);
    topTexture_->release();
    ceTexture_->release();
    bottomTexture_->release();
    vbo_.release();
    program_.release();

    f->glDisable(GL_CULL_FACE);
    f->glDisable(GL_DEPTH_TEST);
}
