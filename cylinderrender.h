#ifndef CYLINDERRENDER_H
#define CYLINDERRENDER_H

#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>
#include <QOpenGLExtraFunctions>
#define PI 3.14159265f
class CylinderRender
{
public:
    CylinderRender() = default;
    void initsize(QImage &ce,QImage &top,QImage &bottom,float r,float h);
    void render(QOpenGLExtraFunctions *f,QMatrix4x4 &pMatrix,QMatrix4x4 &vMatrix,QMatrix4x4 &mMatrix,QVector3D &light,QVector3D &camera);

private:
    QOpenGLShaderProgram program_;
    QOpenGLBuffer vbo_;
    QVector<GLfloat> vertVec_,textVec_,normalVec_;
    QVector<GLfloat> ceVec,ceTextVec,ceNorVec,topVec,topTexVec,topNorVec,bottomVec,bottomTexVec,bottomNorVec;
    QOpenGLTexture *ceTexture_{nullptr},*topTexture_{nullptr},*bottomTexture_{nullptr};
};

#endif // CYLINDERRENDER_H
